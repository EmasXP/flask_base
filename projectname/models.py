from passlib.hash import pbkdf2_sha256
from flask_login import UserMixin
from peewee import AutoField, CharField
from .database import BaseModel
from .security import pwd_context, PasswordCheck


class User(BaseModel, UserMixin):
    id = AutoField()
    username = CharField()
    password = CharField()

    def __repr__(self):
        return "<User(%r, %r)>" % (self.id, self.username)

    def hash_password(self, password):
        return pwd_context.hash(password)

    def verify_password(self, password) -> PasswordCheck:
        return PasswordCheck(password, self.password)

    def get_id(self):
        return str(self.id)
