from flask import Blueprint
from flask_login import login_required

bp = Blueprint("welcome", __name__)


@bp.route("/")
def index():
    return "Hello, world!"


@bp.route("/secret")
@login_required
def index():
    return "This is secret!"
