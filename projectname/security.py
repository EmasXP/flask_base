from passlib.context import CryptContext

# https://passlib.readthedocs.io/en/stable/narr/quickstart.html#creating-and-using-a-cryptcontext
pwd_context = CryptContext(
    schemes=["argon2", "pbkdf2_sha256"],
    deprecated="auto",
)


class PasswordCheck:
    def __init__(self, password, hash):
        self.password = password
        self.hash = hash

    def correct(self):
        return pwd_context.verify(self.password, self.hash)

    def needs_update(self):
        return pwd_context.needs_update(self.hash)
