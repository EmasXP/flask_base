# https://www.patricksoftwareblog.com/structuring-a-flask-project/

import os
import json
from flask import Flask, url_for
import appdirs


def create_app() -> Flask:
    app = Flask(__name__)

    app.config.from_object("projectname.settings")
    user_config_dir_settings_file = os.path.join(
        appdirs.user_config_dir("projectname"), "settings.json"
    )
    if os.path.exists(user_config_dir_settings_file):
        app.config.from_file(user_config_dir_settings_file, load=json.load)

    with app.app_context():
        from .database import db

        @app.before_request
        def before_request():
            db.connect()

        @app.teardown_request
        def teardown_request(exc):
            if not db.is_closed():
                db.close()

        init_auth(app)
        register_blueprints(app)

    return app


def init_auth(app: Flask):
    from typing import Optional
    from flask import request, render_template, flash, redirect
    from flask_login import LoginManager, login_user, login_required, logout_user
    from .models import User

    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = "login"

    @login_manager.user_loader
    def load_user(user_id):
        return User.get_or_none(User.id == user_id)

    @app.route("/login", methods=["GET", "POST"])
    def login():
        errors = []

        if request.method == "POST":
            def check_user():
                username = request.form.get("username")
                password = request.form.get("password")

                user: Optional[User] = User.get_or_none(User.username % username)

                if not user:
                    return False

                verify = user.verify_password(password)

                if not verify.correct():
                    return False

                login_user(user, remember=True)
                flash("Logged in successfully.")

                if verify.needs_update():
                    user.password = user.hash_password(password)
                    user.save()

                return True

            if check_user():
                # next = request.args.get('next')
                # next_is_valid should check if the user has valid
                # permission to access the `next` url
                # if not next_is_valid(next):
                #    return redirect("/")

                return redirect("/")

            else:
                errors.append("The username or password is not correct.")

        return render_template(
            "login.html.j2",
            errors=errors,
        )

    @app.route("/logout")
    @login_required
    def logout():
        logout_user()
        return redirect(url_for(login_manager.login_view))


def register_blueprints(app: Flask):
    from .blueprints import welcome

    app.register_blueprint(welcome.bp, url_prefix="/")
