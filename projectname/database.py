from flask import current_app
from peewee import Model
from playhouse.apsw_ext import APSWDatabase


db = APSWDatabase(
    current_app.config["DATABASE"],
    pragmas={
        "journal_mode": "wal",
        "foreign_keys": 1,
        "ignore_check_constraints": 0,
    },
)


def make_table_name(model_class):
    # https://web2.uvcs.uvic.ca/courses/elc/studyzone/330/grammar/irrplu.htm
    # https://stackoverflow.com/questions/18902608/generating-the-plural-form-of-a-noun/19018986
    model_name = model_class.__name__
    word = model_name.lower()
    if word.endswith("fe"):
        # knife -> knives
        return word[:-2] + "ves"
    elif word.endswith("us"):
        # knife -> knives
        return word[:-2] + "i"
    elif word.endswith("is"):
        # knife -> knives
        return word[:-2] + "es"
    elif word.endswith("y"):
        # activity -> activities
        return word[:-1] + "ies"
    elif word.endswith("o"):
        # potato -> potatoes
        return word + "es"
    elif word.endswith("f"):
        # wolf -> wolves
        return word[:-1] + "ves"
    return word + "s"


class BaseModel(Model):
    class Meta:
        database = db
        table_function = make_table_name
