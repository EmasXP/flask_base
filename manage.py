import click
from projectname import create_app

app = create_app()


@app.cli.command()
@click.option("--host", "-h", default="localhost", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def dev(host, port):
    import logging

    logger = logging.getLogger("peewee")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

    app.run(debug=True, host=host, port=port)


@app.cli.command()
@click.option("--host", "-h", default="0.0.0.0", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def bjoern(host, port):
    import bjoern

    bjoern.run(app, host, port)


@app.cli.command()
@click.option("--host", "-h", default="0.0.0.0", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def meinheld(host, port):
    from meinheld import server

    server.listen((host, port))
    server.set_access_logger(None)
    server.run(app)


@app.cli.command()
@click.option("--host", "-h", default="0.0.0.0", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def twisted(host, port):
    from twisted.web.wsgi import WSGIResource
    from twisted.internet import reactor
    from twisted.web.server import Site

    resource = WSGIResource(reactor, reactor.getThreadPool(), app)
    reactor.listenTCP(port, Site(resource), interface=host)
    reactor.run()


@app.cli.command()
@click.option("--host", "-h", default="0.0.0.0", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def tornado(host, port):
    import tornado.wsgi, tornado.httpserver, tornado.ioloop

    container = tornado.wsgi.WSGIContainer(app)
    server = tornado.httpserver.HTTPServer(container)
    server.listen(port=port, address=host)
    tornado.ioloop.IOLoop.instance().start()


@app.cli.command()
@click.option("--host", "-h", default="0.0.0.0", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def gevent(host, port):
    from gevent.pywsgi import WSGIServer

    WSGIServer((host, port), app, log=None).serve_forever()


@app.cli.command()
@click.option("--host", "-h", default="0.0.0.0", show_default=True)
@click.option("--port", "-p", default=8080, show_default=True)
def cheroot(host, port):
    from cheroot import wsgi

    server = wsgi.Server((host, port), app)
    server.start()


if __name__ == "__main__":
    app.cli()
