# flask_base

This is the Flask structure I use for my projects. Whenever I create a new project I make a copy of this structure and make the modifications needed. In all honesty, I usually start off with a quick one-file Flask app, and realize I need some structure - and _then_ I turn to this code.

This repository is basically just for myself, but you are of course welcome to use it if you like!

## Usage

Make a copy of the structure and do the following changes:

- wsgi.py
  - Change name of projectname in import
- manage.py
  - Change name of projectname in import
  - Change address of server(s)
  - Delete commands not desired
  - Delete undesired code or logging
- projectname (folder)
  - Rename folder
- projectname/\_\_init\_\_.py
  - Update configuration
  - Delete undesired code
  - Anything more?
- projectname/database.py
  - Anything?
- projectname/models.py
  - Anything?
- projectname/settings.py
  - Anything?

## TODO

- Render template in welcome.py
- Default template
- Exemple task i manage.py (that uses the app context)
- Auth templates
- Migrations

